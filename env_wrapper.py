import argparse
import labels_compare

parser = argparse.ArgumentParser()
parser.add_argument('-l', action='store', required=False, type=str, default='./locales', help='Path to list of locales (xx/yy format, one per each line)')
parser.add_argument('URL1', type=str, action='store', help='URL address of first environment to compare')
parser.add_argument('URL2', type=str, action='store', help='URL address of second environment to compare')
args = parser.parse_args()

with open(args.l) as locales:
    num_of_diff = 0
    for i, line in enumerate(locales):
        URL1 = '{}/{}/search/'.format(args.URL1, line.rstrip())
        URL2 = '{}/{}/search/'.format(args.URL2, line.rstrip())

        num_of_diff += labels_compare.main(URL1, URL2)
        
    if num_of_diff > 0:
        print('!!! There were {} difference(s) between {} and {} environments in all locales provided'.format(num_of_diff, args.URL1, args.URL2))
    else:
        print('<3 There were no differences between {} and {} in all locales provided'.format(args.URL1, args.URL2))
