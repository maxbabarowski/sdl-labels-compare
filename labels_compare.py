import argparse
from urllib.parse import urlparse
import requests
import json

CODE_KEY = 'labelCode'
VALUE_KEY = 'translation'
API_ENDPOINT_TEMPLATES = {
    'multilevel': {
        '{0}/api/labels/', '{0}/api/subcategories'
    },
    'singlelevel': {
        '{0}/api/facetsLabels/', '{0}/api/categories/'
    }
}

def compare_dict(url1, url2):
    print('Comparing two endpoints:\n\t-> {}\n\t-> {}\n'.format(url1, url2))

    total_differences = 0

    json1 = get_json(url1)
    json2 = get_json(url2)

    json1_unique_labels = list_diff(json1, json2)
    json2_unique_labels = list_diff(json2, json1)

    total_differences += len(json1_unique_labels)
    total_differences += len(json2_unique_labels)
    print('Labels present in source 1 and not present in source 2:\n{}\n'.format(json1_unique_labels))
    print('Labels present in source 2 and not present in source 1:\n{}\n'.format(json2_unique_labels))

    different_translation_labels = list(filter(lambda x: json1[x] != json2[x], common_labels(json1, json2)))
    
    num_of_translation_differences = len(different_translation_labels)
    if num_of_translation_differences > 0:
        total_differences += num_of_translation_differences
        print('Found {} translation difference(s) in {}'.format(num_of_translation_differences, different_translation_labels))
        [print('{}\t{}\n'.format(json1[x], json2[x])) for x in different_translation_labels]
    else:
        print('No differences in translations were found between two sources provided\n')
    
    print('---\n')
    return total_differences

def compare_nested_dict(url1, url2):
    print('Comparing two endpoints:\n\t-> {}\n\t-> {}\n'.format(url1, url2))

    total_differences = 0

    json1 = get_json(url1)
    json2 = get_json(url2)
    
    json1_labels = extract_sublist(json1, CODE_KEY)
    json2_labels = extract_sublist(json2, CODE_KEY)

    json1_unique_labels = list_diff(json1_labels, json2_labels)
    json2_unique_labels = list_diff(json2_labels, json1_labels)

    total_differences += len(json1_unique_labels)
    total_differences += len(json2_unique_labels)
    print('Labels present in source 1 and not present in source 2:\n{}\n'.format(json1_unique_labels))
    print('Labels present in source 2 and not present in source 1:\n{}\n'.format(json2_unique_labels))

    different_translation_labels = list(filter(lambda x: get_translation(json1, x) != get_translation(json2, x), common_labels(json1_labels, json2_labels)))

    num_of_translation_differences = len(different_translation_labels)
    if num_of_translation_differences > 0:
        total_differences += num_of_translation_differences
        print('Found {} translation difference(s) in {}:\nSource 1:\tSource 2:'.format(num_of_translation_differences, different_translation_labels))
        [print('{}\t{}\n'.format(get_translation(json1, x), get_translation(json2, x))) for x in different_translation_labels]
    else:
        print('No differences in translations were found between two sources provided\n')

    print('---\n')
    return total_differences

def get_json(url_):
    resp = requests.get(url=url_)
    return resp.json()

def get_translation(json, key):
    for translation in json:
        if translation[CODE_KEY] == key:
            return translation[VALUE_KEY]

    raise ValueError('No translation found for key: {}'.format(key))

def common_labels(list1, list2):
    return list_diff(list1, list_diff(list1, list2))

def list_diff(list1, list2):
    return list(filter(lambda x: x not in list2, list1))

def extract_sublist(list_, key):
    return list(map(lambda x: x[key], list_))

def main(URL1=str, URL2=str, direct=False, single=False, multi=False):
    diff_counter = 0

    if direct == False:
        print('Checking environments:\n\tSource 1 -> {}\n\tSource 2 -> {}\n-----\n'.format(URL1, URL2))
        for endpoint in API_ENDPOINT_TEMPLATES['multilevel']:
            URL1_endpoint = endpoint.format(URL1)
            URL2_endpoint = endpoint.format(URL2)
            diff_counter += compare_nested_dict(URL1_endpoint, URL2_endpoint)
        
        for endpoint in API_ENDPOINT_TEMPLATES['singlelevel']:
            URL1_endpoint = endpoint.format(URL1)
            URL2_endpoint = endpoint.format(URL2)
            diff_counter += compare_dict(URL1_endpoint, URL2_endpoint)
    else:
        if single:
            diff_counter += compare_dict(URL1, URL2)
        elif multi:
            diff_counter += compare_nested_dict(URL1, URL2)

    if diff_counter > 0:
        print('!! {} difference(s) was/were found between two environments'.format(diff_counter))
    else:
        print('There were no differences found between two environments')
    
    print('----------\n\n')
    return diff_counter

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-D', '--direct', action='store_true', help='Provided direct URLs to API endpoints')
    direct_type = parser.add_mutually_exclusive_group()
    direct_type.add_argument('--single', action='store_true', help='Provided API structure is simple and non-nested')
    direct_type.add_argument('--multi', action='store_true', help='Provided API structure is nested inside a list')
    parser.add_argument('URL1', type=str, action='store', help='URL address of first environment to compare')
    parser.add_argument('URL2', type=str, action='store', help='URL address of second environment to compare')

    args = parser.parse_args()

    if args.direct and (args.single is False and args.multi is False):
        parser.error('-D or --direct requires --single or --multi')

    main(args.URL1, args.URL2, args.direct, args.single, args.multi)
    exit(0)
