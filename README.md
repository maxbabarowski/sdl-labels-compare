# sdl_labels_compare

## Prerequisites
- Python >3.7
- virtualenv

## Installation
### Windows
```
> git clone https://bitbucket.org/maxbabarowski/sdl-labels-compare/
> cd sdl-labels-compare
> virtualenv .venv
> .\.venv\Scripts\activate.bat
> pip install -r requirements.txt
```
### Linux
```
$ git clone https://bitbucket.org/maxbabarowski/sdl-labels-compare/
$ cd sdl-labels-compare
$ virtualenv .venv
$ source .venv/bin/activate
$ pip install -r requirements.txt
```

## Usage
### env_wrapper.py
```
usage: env_wrapper.py [-h] [-l L] URL1 URL2

positional arguments:
  URL1        URL address of first environment to compare
  URL2        URL address of second environment to compare

optional arguments:
  -h, --help  show this help message and exit
  -l L        Path to list of locales (xx/yy format, one per each line)
```
Required arguments `URL1` and `URL2` should be provided in this format: `http://example.com:80`. Default path to file with list of locales is `./locales`. You can override it using `-l <path>` option. The list should contain exactly one locale per line, each in this format: `xx/yy`.

Use this script to check every country based on locales list in two given environments. This script is a higher level tool, which uses [labels_compare](#labelscomparepy) against all the countries provided.

### labels_compare.py
```
usage: labels_compare.py [-h] [-D] [--single | --multi] URL1 URL2

positional arguments:
  URL1          URL address of first environment to compare
  URL2          URL address of second environment to compare

optional arguments:
  -h, --help    show this help message and exit
  -D, --direct  Provided direct URLs to API endpoints
  --single      Provided API structure is simple and non-nested
  --multi       Provided API structure is nested inside a list
```
Required arguments `URL1` and `URL2` should be provided in this format: `http://example.com:80/xx/yy/search`

#### API endpoints configuration
The API endpoint addresses are defined in the `API_ENDPOINT_TEMPLATES` variable. Default values are:
- `/api/labels/`
- `/api/subcategories/`
- `/api/facetsLabels/`
- `/api/categories/`

You can override this setting by running script with `-D` flag and specyfing URLs pointing directly to API endpoints of two environments. `--single` or `--multi` flags are required if `-D` is used used. Their purpose is to indicate a structure of API response. 

For example, `--single` option is appropiate for using with response coming from `/api/categories` endpoint,
```
{
    "key": "value"
}
```
while `--multi` should be used against response format provided by `/api/labels/`
```
[
    {
        "labelCode": "CODE",
        "languageCode": "xx_YY",
        "translation": "Sample text"
    }
]
```